<?php
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"	"http://www.w3.org/TR/html4/strict.dtd"> 
<html lang="<?php print $language ?>">
  <head>
    <title><?php echo $head_title ?></title>
    <?php echo $head, $styles, $scripts ?>
    <!--[if lt IE 7]>
    <style type="text/css" media="all">@import "<?php echo base_path() . path_to_theme() ?>/fix-ie.css";</style>
    <![endif]-->
  </head>
  <body >
    <div id="<?php phptemplate_page_width() ?>" class="<?php phptemplate_page_template( $sidebar_left, $sidebar_right ) ?>">
      <div id="hd">
        <?php
          echo $header;
          
          // Prepare header
          $site_fields = array();
          
          if ($site_name)   $site_fields[] = check_plain( $site_name );
          if ($site_slogan) $site_fields[] = check_plain( $site_slogan );  
            
          $site_title     = implode( ' ', $site_fields );
          $site_fields[0] = '<span>'. $site_fields[0] .'</span>';
          $site_html      = implode( ' ', $site_fields );

          if ( $logo || $site_title ) {
            echo '<h1><a href="'. check_url( $base_path ) .'" title="'. $site_title .'">';
            if ( $logo ) echo '<img src="'. check_url( $logo ) .'" alt="'. $site_title .'" id="logo" />';
            echo $site_html .'</a></h1>';
          }
        ?>
        </div>

      <div id="nav">
        <?php
          if ( isset( $primary_links) )   echo theme('links', $primary_links, array('class' => 'links primary-links'));
          if ( isset( $secondary_links) ) echo theme('links', $secondary_links, array('class' => 'links secondary-links'));
        ?>
      </div>

      <div id="bd">        

        <div id="yui-main">
          <div class="yui-b">
            <?php if ( $sidebar_left && $sidebar_right ): ?>
              <div class="yui-g"> 
                <div id="sidebar-left" class="yui-u first">
                  <?php
                    echo $search_box ? $search_box : '';
                    echo $sidebar_left;
                  ?>
                </div>
                <div class="yui-u">
            <?php endif ?>        
          
            <?php
              echo $breadcrumb  ? $breadcrumb : '';
              echo $mission     ? '<div id="mission">'. $mission .'</div>' : '';
              echo $tabs        ? '<div id="tabs-wrapper" class="clear-block">' : '';
              echo $title       ? '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>' : '';
              echo $tabs        ? $tabs .'</div>' : '';
              echo isset($tabs2)? $tabs2 : '';
              echo $help        ? $help : '';
              echo $messages    ? $messages : '';
  					  echo $content;
            ?>
          
            <?php if ( $sidebar_left && $sidebar_right ): ?>
              </div>
              </div>
            <?php endif?>
          </div>
        </div>

        <?php if ( $sidebar_left && $sidebar_right == ''): ?>
          <div id="sidebar-left" class="yui-b">
            <?php
              echo $search_box ? $search_box : '';
              echo $sidebar_left;
            ?>
          </div>
        <?php endif ?>
        
        <?php if ( $sidebar_right ): ?>
          <div id="sidebar-right" class="yui-b">
            <?php if (!$sidebar_left && $search_box): ?><?php print $search_box ?><?php endif; ?>
            <?php print $sidebar_right ?>
          </div>
        <?php endif; ?>
        
      </div>
      <div id="ft"><?php echo $footer_message, $feed_icons ?></div>
    </div>

  <?php echo $closure ?>
  </body>
</html>