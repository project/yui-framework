<?php
?>
<?php phptemplate_comment_wrapper(NULL, $node->type); ?>

<div id="node-<?php echo $node->nid; ?>" class="node<?php if ($sticky) { echo ' sticky'; } ?><?php if (!$status) { echo ' node-unpublished'; } ?>">

<?php echo $picture ?>

<?php if ($page == 0): ?>
  <h2><a href="<?php echo $node_url ?>" title="<?php echo $title ?>"><?php echo $title ?></a></h2>
<?php endif; ?>

  <?php if ($submitted): ?>
    <span class="submitted"><?php echo t('!date — !username', array('!username' => theme('username', $node), '!date' => format_date($node->created))); ?></span>
  <?php endif; ?>

  <div class="content">
    <?php echo $content ?>
  </div>

    <div class="meta">
    <?php if ($taxonomy): ?>
      <div class="terms"><?php echo $terms ?></div>
    <?php endif;?>

    <?php if ($links): ?>
      <div class="links"><?php echo $links; ?></div>
    <?php endif; ?>
    </div>

</div>