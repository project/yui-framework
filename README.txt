
Drupal 5.7 YUI Framework theme
Created bt Xavier Briand > user xavierbriand
Based on by Andre Griffin > user andregriffin


YUI Famework is a theme based on Framework Theme.
It intends to provide a full cross-browser compatible framework theme bases on Yahoo! UI CSS jobs.

Framework is not intended to be used as is. Please edit freely.


>>> INSTALL to sites/all/themes


>>> SUPPORT

If you have questions or problems, check the issue list before submitting a new issue: 
http://drupal.org/project/issues/yui-framework

For general support, please refer to:
http://drupal.org/support

To contact me directly, please visit: 
http://drupal.org/user/252408/contact