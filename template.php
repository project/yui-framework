<?php
?>
<?php
/**
 * Drupal 5.7 Yahoo: UI Framework - Created Xavier Briand
 * From Drupal 5.7 Framework - Created Andre Griffin
 * Template.php
 */

/**
 * Sets the page width according to YUI specification
 *
 * http://developer.yahoo.com/yui/examples/grids/grids-doc.html 
 */
function phptemplate_page_width() {
  echo 'doc'; //750px width, centered
  //echo 'doc2'; //950px width, centered
  //echo 'doc3'; //100% width
  //echo 'doc4'; //974px width, centered
}


/**
 * Sets the page template
 *
 * Source-Order Independence for SEO
 */
function phptemplate_page_template($sidebar_left, $sidebar_right) {
  $template = '';
  
  switch( true ) {
    case $sidebar_left != '' &&  $sidebar_right == '' :
      $template = 'yui-t1'; //160px
      //$template = 'yui-t2'; //180px
      //$template = 'yui-t3'; //300px
      break;
    case $sidebar_right != '' :
      //$template = 'yui-t4'; //180px
      $template = 'yui-t5'; //240px
      //$template = 'yui-t6'; //300px
      break;
  }
  
  echo $template;
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' › ', $breadcrumb) .'</div>';
  }
}

/**
 * Allow themable wrapping of all comments.
 */
function phptemplate_comment_wrapper($content, $type = null) {
  static $node_type;
  if (isset($type)) $node_type = $type;

  if (!$content || $node_type == 'forum') {
    return '<div id="comments">'. $content . '</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}

/**
 * Override or insert PHPTemplate variables into the templates.
 */
function _phptemplate_variables($hook, $vars) {
  if ($hook == 'page') {

    if ($secondary = menu_secondary_local_tasks()) {
      $output = '<span class="clear"></span>';
      $output .= "<ul class=\"tabs secondary\">\n". $secondary ."</ul>\n";
      $vars['tabs2'] = $output;
    }

    // Hook into color.module
    if (module_exists('color')) {
      _color_page_alter($vars);
    }
    return $vars;
  }
  return array();
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks() {
  $output = '';

  if ($primary = menu_primary_local_tasks()) {
    $output .= "<ul class=\"tabs primary\">\n". $primary ."</ul>\n";
  }

  return $output;
}
